package com.doranco.multitiers.entity;

import java.io.Serializable;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "l_user")
public class User extends Identifier implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 3946495164757603011L;

	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	private String firstName;
	
	@NotNull
	private String lastName;

	@NotNull
	private String userName;

	@NotNull
	private String password;

	private boolean isAdmin = false;

	@OneToMany(mappedBy = "user")
	private Set<Order> order;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private Set<Note> notes;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
	private Set<Viewing> viewings;

	public Set<Viewing> getViewings() {
		return viewings;
	}

	public void setViewings(Set<Viewing> viewings) {
		this.viewings = viewings;
	}

	public Set<Note> getNotes() {
		return notes;
	}

	public void setNotes(Set<Note> notes) {
		this.notes = notes;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isAdmin() {
		return isAdmin;
	}

	public void setAdmin(boolean isAdmin) {
		this.isAdmin = isAdmin;
	}

}
