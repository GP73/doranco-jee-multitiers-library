package com.doranco.multitiers.entity;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "l_order_line")
@IdClass(IdOrderLine.class)
public class OrderLine implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5202710545616020894L;

	public OrderLine() {
		super();
		// TODO Auto-generated constructor stub
	}

	@Id
	@ManyToOne
	private Order order;

	@Id
	@OneToOne
	private Book book;

	public Order getOrder() {
		return order;
	}

	public void setOrder(Order order) {
		this.order = order;
	}

	public Book getBook() {
		return book;
	}

	public void setBook(Book book) {
		this.book = book;
	}
	
	
	

	

}
