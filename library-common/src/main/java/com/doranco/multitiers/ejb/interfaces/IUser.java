package com.doranco.multitiers.ejb.interfaces;

import javax.ejb.Remote;

import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Remote
public interface IUser {

	public User suscribe(User user) throws LibraryException;

	public User connect(String userName, String password) throws LibraryException;
	
	public Page<User> getUsers(int pageNumber, int pageSize) throws LibraryException;

}
