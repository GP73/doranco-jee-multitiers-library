package com.doranco.multitiers.utils;

import java.util.List;

public class Page<T> {

	private long totalCount;
	private List<T> content;

	public long getTotalCount() {
		return totalCount;
	}

	public void setTotalCount(long totalCount) {
		this.totalCount = totalCount;
	}

	public List<T> getContent() {
		return content;
	}

	public void setContent(List<T> content) {
		this.content = content;
	}

}
