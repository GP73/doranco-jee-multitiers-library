package com.doranco.multitiers.dao.test;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import javax.ejb.EJB;
import javax.ejb.EJBException;

import org.apache.log4j.Logger;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.junit.InSequence;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import com.doranco.multitiers.dao.GenericDAO;
import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.entity.User;

@RunWith(Arquillian.class)
public class UserDAOTest {

	@Deployment
	public static JavaArchive createDeployment() {
		return ShrinkWrap.create(JavaArchive.class).addClasses(UserDAO.class, GenericDAO.class);
	}

	@EJB
	private UserDAO dao;

	private static Logger logger = Logger.getLogger(UserDAOTest.class);

	@Test
	@InSequence(1)
	public final void shouldCreateAUser() {

		try {
			User user = new User();
			user.setFirstName("philippe");
			user.setLastName("SIMO");
			user.setUserName("philippesimo");
			user.setPassword("philippesimo");

			dao.create(user);

		} catch (Exception e) {
			logger.error("Error = ", e);
			fail("Ne devrait pas retourner d'exception ");
		}

	}

	@Test
	@InSequence(2)
	public final void shouldFindAUserWithAGoodPassword() {
		try {

			User connectedUser = dao.findByUserNameAndPassword("philippesimo", "philippesimo");

			assertNotNull(connectedUser);

		} catch (Exception e) {
			logger.error("Error = ", e);
			fail("Ne devrait pas retourner d'exception lors de la récupération d'un user avec de bon identifiants");
		}

	}

	@Test
	@InSequence(3)
	public final void shouldNotFindAUserWithABadPassword() {
		User user = null;
		try {

			user = dao.findByUserNameAndPassword("philippesimo", "philippe");

		} catch (EJBException nre) {

		}

		assertNull(user);

	}

	//@Test
	public final void testFind() {
		fail("Not yet implemented"); // TODO
	}

	//@Test
	public final void testUpdate() {
		fail("Not yet implemented"); // TODO
	}

}
