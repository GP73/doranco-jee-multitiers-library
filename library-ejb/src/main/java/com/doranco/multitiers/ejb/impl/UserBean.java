package com.doranco.multitiers.ejb.impl;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.NoResultException;

import org.apache.log4j.Logger;

import com.doranco.multitiers.dao.UserDAO;
import com.doranco.multitiers.ejb.interfaces.IUser;
import com.doranco.multitiers.entity.User;
import com.doranco.multitiers.exceptions.LibraryException;
import com.doranco.multitiers.utils.Page;

@Stateless
public class UserBean implements IUser {

	Logger logger = Logger.getLogger(UserBean.class);

	@EJB
	UserDAO userDAO;

	@Override
	public User suscribe(User user) throws LibraryException {

		try {
			user = userDAO.create(user);
		} catch (Exception e) {
			logger.error("Problème lors de l'inscription d'un utilisateur", e);
			throw new LibraryException();
		}

		return user;

	}

	@Override
	public User connect(String userName, String password) throws LibraryException {
		User user = null;
		try {

			user = userDAO.findByUserNameAndPassword(userName, password);

		} catch (NoResultException nre) {
			logger.error("Impossible de trouver un tel utilisateur", nre);
			// TODO throw new UserNotFoundException();
			throw new LibraryException();
		} catch (Exception e) {
			logger.error("Problème lors de la connexion d'un utilisateur", e);
			throw new LibraryException();
		}

		return user;

	}

	@Override
	public Page<User> getUsers(int pageNumber, int pageSize) throws LibraryException {
		Page<User> page=null;
		try {
			page = userDAO.find(User.class, pageNumber	, pageSize);
			
		} catch (Exception e) {
			logger.error("Problème lors de la récupération des utilisateurs", e);
			throw new LibraryException();
		}
		return page;
	}

}
