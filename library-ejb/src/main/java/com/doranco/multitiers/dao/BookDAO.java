package com.doranco.multitiers.dao;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import com.doranco.multitiers.entity.Book;

@Stateless
@LocalBean
public class BookDAO extends GenericDAO<Book> {

}
